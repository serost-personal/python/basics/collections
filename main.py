# Usefull resources:
# https://medium.com/analytics-vidhya/python-lambda-map-filter-reduce-and-zip-functions-73556a86a454

from functools import reduce
from typing import Callable


def main():
    # basically the same thing

    def sum1(x: int, y: int) -> int:
        return x + y

    sum2: Callable[[int, int], int] = lambda x, y: x + y

    print(sum1(1, 2), sum2(1, 2))

    # from what i understand
    # map() is a __init__ for <class 'map'>
    # list() may take <class 'map'> as an argument

    # map() - do "function" for each element in a given collection(s)

    l: list[str] = ["hello", "world", "good", "sir"]

    lens: list[int] = list(map(lambda i: len(i), l))
    print(l, lens, sep="\n")

    lens2: list[int] = [len(i) for i in l]
    print("Others way:", l, lens2, sep="\n")

    # filter() removes all elements, that does not follow given predicate

    l: list[str] = ["deb", "beb", "hello", "world", "one", "two", "three"]
    l2: list[str] = list(filter(lambda x: len(x) > 3, l))
    print(l2)

    # reduce() runs like this, given [1, 2, 3, 4, ...]
    #
    # res = 1
    # func(res, 2) -> res
    # func(res, 3) -> res
    # fund(res, 4) -> res
    # ... and so on

    def cat_strings(res: str, next: str):
        print(res, next)
        return res + next

    l: list[str] = ["Hello", ", ", "world", "!"]
    s: str = reduce(cat_strings, l)
    print(s)

    first_names: list[str] = ["willy", "bill", "anna", "beb"]
    last_names: list[str] = ["doe", "smith", "idk", "deb"]
    ages: list[int] = [15, 45, 23]

    # ("beb", "deb", ???) will not be present, since
    # no appropriate age at ages[3] found.
    combined = list(zip(first_names, last_names, ages))

    print(combined)


if __name__ == "__main__":
    main()
